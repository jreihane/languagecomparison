# LanguageComparison

This project is aimed to compare programming languages to help others understand what are the pros and cons of the language they're going to use in their projects.

To start, I'm going to 

* create a sub-project for each language and write small programms to demonstrate their syntax
* evaluate their performance for each statement

Next phase would be to write a one-page document for each language for setup instructions and evaluate the "difficulty" of their configuration.

If things go well, I'll evaluate how "maintenable" each program is. However, I haven't found a good way to do so.
